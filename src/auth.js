const md5 = require('md5');
var User = require('./model.js').User;
var Profile = require('./model.js').Profile;
const cookieKey = 'sid';

const sessionUser = {currUser: 'Kanye', currId: 1};

const getLoggedInUser = () => {
    return sessionUser.currUser;
}

const getLoggedInUserId = () => {
    return sessionUser.currId;
}

// curl -H 'Content-Type:application/json' -X POST -d '{"username": "Kanye", "password":"kim-k-west"}' localhost:3000/login
const login = (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!username || !password) { // Bad Request
      res.sendStatus(400);
      return;
  }

  User.findOne({username: username}).exec(function (err, items) {
      if (items.hash !== getHash(password, items.salt)) {
        res.sendStatus(401)
      }

      const sessionKey = md5(password + items.salt + username);
      sessionUser[sessionKey] = items;
      sessionUser[currUser] = username;

      res.cookie(cookieKey, sessionKey, { maxAge: 3600*1000, httpOnly: true});
      res.send({username: username, result: 'success'})
  });
}

const isLoggedIn = (req, res, next) => {
  const sid = req.cookies[cookieKey];

  if (!sid) { // unauthorized
    return res.sendStatus(401);
  }

  const username = sessionUser[sid].username;

  if (username) {
    req.username = username;
    next();
  } else {
    res.sendStatus(401);
  }
}

const logout = (req, res) => {
    sessionUser[sessionKey] = null;
    sessionUser[sessionUser.currUser] = null;
    res.cookie(cookieKey, '', {expires: new Date(0)});
    res.send('OK');
}

const getHash = (password, salt) => {
  return md5(password + salt);
}

const getSalt = () => {
  let salt = '';
  let alpha = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ';

  for (let i = 0; i < 10; i++) {
    salt += alpha.charAt(Math.ceil(Math.random * 52));
  }

  return salt
}

const register = (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const dob = req.body.dob;
    const zipcode = req.body.zipcode;

    if (!username || !password || !email || !dob || !zipcode) {
        res.sendStatus(400);
        return;
    }

    new Profile({
        username: username, status: 'I just joined!', following: [], email: email,
        zipcode: zipcode,
        picture: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/New_user_icon-01.svg/2000px-New_user_icon-01.svg.png'
    })
        .save();

    const salt = getSalt();
    new User({username: username, password: getHash(password, salt), salt: salt}).save()
}

exports.login = login;
exports.logout = logout;
exports.isLoggedIn = isLoggedIn;
exports.getLoggedInUser = getLoggedInUser;
exports.register = register;
exports.getLoggedInUserId = getLoggedInUserId