var Profile = require('./model.js').Profile;
const authResp = require('./auth.js');

const getFollowing = (req, res) => {
    const id = req.params.id || authResp.getLoggedInUserId();

    Profile.findOne({_id: id}).exec(function (err, items) {
        res.send({username: items.username, following: items.following});
    });
}

const putFollowing = (req, res) => {
    const id = req.params.id;

    if (!id) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({_id: authResp.getLoggedInUserId()}).exec(function (err, items) {
        items.following.push(id);
        items.save();
        res.send({username: items.username, following: items.following});
    });
}

const deleteFollowing = (req, res) => {
    const id = req.params.id;

    if (!id) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({_id: authResp.getLoggedInUserId()}).exec(function (err, items) {
        const index = items.following.indexOf(id);
        if (index < 0) {
            res.sendStatus(400);
            return;
        }

        items.following.splice(id, 1);
        items.save();
        res.send({username: items.username, following: items.following});
    });
}

exports.getFollowing = getFollowing;
exports.putFollowing = putFollowing;
exports.deleteFollowing = deleteFollowing;