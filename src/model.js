// this is model.js 
var mongoose = require('mongoose');
require('../index.js');

var articleSchema = new mongoose.Schema({
	_id: Number, author: String, img: String, date: Date, text: String,
	comments: [ {commentId: Number, author: String, date: Date, text: String} ]
});

var userSchema = new mongoose.Schema({
	username: String, salt: String, hash: String
});

var profileSchema = new mongoose.Schema({
    _id: Number, username: String, status: String, following: [Number], email: String,
	zipcode: String, img: String, dob: Date
});

exports.Article = mongoose.model('article', articleSchema);
exports.User = mongoose.model('user', userSchema);
exports.Profile = mongoose.model('profile', profileSchema);