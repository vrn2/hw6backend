var Article = require('./model.js').Article;
const authResp = require('./auth.js');

function findByAuthor(author, callback) {
    Article.find({ author: author }).exec(function(err, items) {
        callback(items);
    });
}

function findById(id, callback) {
    Article.find({ _id: id }).exec(function(err, items) {
        callback(items);
    });
}

function getAllArticles(callback) {
    Article.find().exec(function (err, items) {
        callback(items);
    })
}

// curl -X GET localhost:3000/articles
const getArticles = (req, res) => {
    const id = req.params.id || '';

    if (id === '') {
        getAllArticles(function(items) {
            res.send({articles: items})
        });
    } else if (isNaN(parseInt(id))) {
        findByAuthor(id, function(items) {
            res.send({articles: items})
        });
    } else {
        findById(parseInt(id), function(items) {
            res.send({articles: items})
        });
    }
}

const getRandomId = () => {
    return Math.floor(Math.random() * 10000);
}

// curl -H 'Content-Type:application/json' -X POST -d '{"text": "2222"}' localhost:3000/article
const postArticle = (req, res) => {
    const text = req.body.text;
    const img = req.body.img || '';

    if (!text) { // Bad Request
        res.sendStatus(400);
        return;
    }

    new Article({_id: getRandomId(), author: authResp.getLoggedInUser(), img: img,
                data: (new Date()).toDateString(), text: text, comments: []}).save();

    Article.find().exec(function (err, items) {
        items.push({_id: getRandomId(), author: authResp.getLoggedInUser(), img: img,
            data: (new Date()).toDateString(), text: text, comments: []});
        res.send({articles: items})
    });
}

// curl -X PUT -H "Content-Type: application/json" -d '{"key1":"value"}' localhost:3000/article
const putArticle = (req, res) => {
    const text = req.body.text;
    const id = parseInt(req.params.id);
    const commentId = parseInt(req.body.commentId) || '';

    if (!text || !id) { // Bad Request
        res.sendStatus(400);
        return;
    }

    if (commentId === '') {
        Article.findOneAndUpdate({_id: id, author: authResp.getLoggedInUser()}, {
            text: text
        }).exec(function (err, items) {
            findById(id, function (items) {
                res.send({articles: items})
            });
        });
    } else if (commentId !== '') {
        if (commentId !== -1) {
            Article.findOne({_id: id},
                function (err, items) {
                    const comment = items.comments.filter((comm) => comm.commentId === commentId)[0];
                    if (comment !== undefined) {
                        comment.text = text;
                        items.save();
                        res.send({articles: items});
                    } else {
                        res.send({});
                    }
                });
        } else {
            Article.update({_id: id},
                {$push: {comments: {author: authResp.getLoggedInUser(), text: text, commentId: getRandomId()}}},
                function (err, items) {
                    findById(id, function (items) {
                        res.send({articles: items})
                    });
                });
        }
    }
}

exports.getArticles = getArticles;
exports.postArticle = postArticle;
exports.putArticle = putArticle;