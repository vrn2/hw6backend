var Profile = require('./model.js').Profile;
const authResp = require('./auth.js');

const getHeadlines = (req, res) => {
    const users = req.params.users ? req.params.users.split(',') : [authResp.getLoggedInUser()];
    Profile.find({}).exec(function (err, items) {
        let results = [];
        items.map((curr) => {
            if (users.length === 0 || users.indexOf(curr.username) !== -1) {
                results.push({username: curr.username, headline: curr.status});
            }
        });
        res.send({headlines: results});
    })
}

const putHeadline = (req, res) => {
    const newStatus = req.body.headline;
    const username = authResp.getLoggedInUser();

    if (!newStatus || !username) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({username: username}).exec(function (err, items) {
        items.status = newStatus;
        items.save();
        res.send({username: username, headline: items.status});
    });
}

const getEmail = (req, res) => {
    const id = parseInt(req.params.user) || authResp.getLoggedInUserId();

    Profile.findOne({_id: id}).exec(function (err, items) {
        res.send({username: items.username, email: items.email});
    });
}

const putEmail = (req, res) => {
    const id = authResp.getLoggedInUserId();
    const newEmail = req.body.email;

    if (!newEmail) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({_id: id}).exec(function (err, items) {
        items.email = newEmail;
        items.save();
        res.send({username: items.username, email: items.email});
    });
}

const getZipcode = (req, res) => {
    const id = parseInt(req.params.user) || authResp.getLoggedInUserId();

    Profile.findOne({_id: id}).exec(function (err, items) {
        res.send({username: items.username, zipcode: items.zipcode});
    });
}

const putZipcode = (req, res) => {
    const id = authResp.getLoggedInUserId();
    const newZip = req.body.zipcode;

    if (!newZip) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({_id: id}).exec(function (err, items) {
        items.zipcode = newZip;
        items.save();
        res.send({username: items.username, zipcode: items.zipcode});
    });
}

const getDob = (req, res) => {
    const id = authResp.getLoggedInUserId();

    Profile.findOne({_id: id}).exec(function (err, items) {
        res.send({username: items.username, dob: new Date(items.dob).getTime()});
    });
}

const putAvatar = (req, res) => {
    const id = authResp.getLoggedInUserId();
    const pic = req.body.img;

    if (!img) {
        res.sendStatus(400);
        return;
    }

    Profile.findOne({_id: id}).exec(function (err, items) {
        items.img = pic;
        items.save();
        res.send({username: items.username, avatar: items.img});
    });
}

const getAvatars = (req, res) => {
    const users = req.params.users ? req.params.user.split(',') : [authResp.getLoggedInUser()];

    Profile.find({}).exec(function (err, items) {
        let results = [];

        items.map((curr) => {
            if (users.length === 0 || users.indexOf(curr.username) !== -1) {
                results.push({username: curr.username, avataor: curr.img});
            }
        });
        res.send({avatars: results});
    })
}

exports.getHeadlines = getHeadlines;
exports.putHeadline = putHeadline;
exports.getEmail = getEmail;
exports.putEmail = putEmail;
exports.getZipcode = getZipcode;
exports.putZipcode = putZipcode;
exports.getDob = getDob;
exports.putAvatar = putAvatar;
exports.getAvatars = getAvatars;