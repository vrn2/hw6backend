/*
 * Test suite for backend
 */
const expect = require('chai').expect;
const fetch = require('isomorphic-fetch');

const url = path => `https://vrn2hw6backend.herokuapp.com${path}`;

describe('Verify put headline', () => {
    it('should have correct status', (done) => {
        fetch(url("/headlines/Kanye"))
        .then(res => {
            expect(res.status).to.eql(200);
            return res.text();
         })
        .then(body => {
            expect(body).to.eql("{\"headlines\":[{\"username\":\"Kanye\",\"headline\":\"I am a God!\"}]}");
        })
        .then(done)
        .catch(done)
    }, 500);

    it('should change status', (done) => {
        fetch(url("/headline"), {
            method: 'PUT',
            body: JSON.stringify({username: "Kanye", headline: "Not a God"}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(res => {
            expect(res.status).to.eql(200);
            return res.text();
        })
        .then(body => {
            expect(body).to.eql("{\"username\":\"Kanye\",\"headline\":\"Not a God\"}");
        })
        .then(done)
        .catch(done)
    }, 500);

    it('should have correct changed status', (done) => {
        fetch(url("/headlines/Kanye"))
        .then(res => {
            expect(res.status).to.eql(200);
            return res.text();
        })
        .then(body => {
            expect(body).to.eql("{\"headlines\":[{\"username\":\"Kanye\",\"headline\":\"Not a God\"}]}");
        })
        .then(done)
        .catch(done)
    }, 500);

    it('should change back to original status', (done) => { // Change back
        fetch(url("/headline"), {
        method: 'PUT',
        body: JSON.stringify({username: "Kanye", headline: "I am a God!"}),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(res => {
        expect(res.status).to.eql(200);
    return res.text();
    })
    .then(body => {
        expect(body).to.eql("{\"username\":\"Kanye\",\"headline\":\"I am a God!\"}");
    })
    .then(done)
        .catch(done)
    }, 500);
});

describe('Verify add article', () => {
    var currCount = 0;

    it('should get all articles', (done) => {
        fetch(url("/articles"))
        .then(res => {
            expect(res.status).to.eql(200);
            return res.text();
        })
        .then(body => {
            return JSON.parse(body).articles;
        })
        .then(body => {
            currCount = body.length;
            expect(body.length).to.be.greaterThan(9);
        })
        .then(done)
        .catch(done)
    }, 500);

    it('should post new article article', (done) => {
        fetch(url("/article"), {
        method: 'POST',
        body: JSON.stringify({text: "New article i posted"}),
        headers: {
            'Content-Type': 'application/json'
        }})
        .then(res => {
            expect(res.status).to.eql(200);
            return res.text();
        })
        .then(body => {
            return JSON.parse(body).articles;
        })
        .then(body => {
            expect(body.filter(art => art.text === "New article i posted").length).to.be.greaterThan(0);
        })
        .then(done)
        .catch(done)
    }, 500);

    it('should increase number of articles', (done) => {
        fetch(url("/articles"))
    .then(res => {
        expect(res.status).to.eql(200);
    return res.text();
    })
    .then(body => {
        return JSON.parse(body).articles;
    })
    .then(body => {
        expect(body.length).to.eql(currCount + 1);
    })
    .then(done)
        .catch(done)
    }, 500);
});
