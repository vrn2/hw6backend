var mongoose = require('mongoose')
const bodyParser = require('body-parser');
const express = require('express');
const articleResp = require('./src/article.js');
const authResp = require('./src/auth.js');
const profileResp = require('./src/profile.js');
const followingResp = require('./src/following.js');

var Article = require('./src/model.js').Article;
var User = require('./src/model.js').User;
var Profile = require('./src/model.js').Profile;

var url = 'mongodb://heroku_j0gtttd4:epe4so1d587m3aal0df30q2pda@ds151955.mlab.com:51955/heroku_j0gtttd4'

if (process.env.MONGOLAB_URI) {
	url = process.env.MONGOLAB_URI;
}

mongoose.connect(url);

mongoose.connection.on('connected', function() {
	console.log('Mongoose connected to ' + url)
});
mongoose.connection.on('error', function(err) {
	console.error('Mongoose connection error: ' + err)
});
mongoose.connection.on('disconnected', function() {
	console.log('Mongoose disconnected')
});

process.once('SIGUSR2', function() {
	shutdown('nodemon restart', function() {
		process.kill(process.pid, 'SIGUSR2')
	})
});
process.on('SIGINT', function() {
	shutdown('app termination', function() {
		process.exit(0)
	})
});
process.on('SIGTERM', function() {
	shutdown('Heroku app shutdown', function() {
		process.exit(0)
	})
});

function shutdown(msg, callback) {
	mongoose.connection.close(function() {
		console.log('Mongoose disconnected through ' + msg);
		callback()
	})
}
const app = express();

app.use(bodyParser.json());

app.get('/articles/:id?', articleResp.getArticles);
app.put('/articles/:id', articleResp.putArticle);
app.post('/article', articleResp.postArticle);

app.post('/login', authResp.login);
app.put('/logout', authResp.isLoggedIn, authResp.logout);
app.post('/register', authResp.register);

app.get('/headlines/:users?', profileResp.getHeadlines);
app.put('/headline', profileResp.putHeadline);

app.get('/email/:user?', profileResp.getEmail);
app.put('/email', profileResp.putEmail);

app.get('/zipcode/:user?', profileResp.getZipcode);
app.put('/zipcode', profileResp.putZipcode);

app.get('/dob', profileResp.getDob);

app.get('/avatars/:user?', profileResp.getAvatars);
app.get('/avatar', profileResp.putAvatar);

app.get('/following/:user?', followingResp.getFollowing);
app.put('/following/:user', followingResp.putFollowing);
app.delete('/following/:user', followingResp.deleteFollowing);

const port = process.env.PORT || 3000;

const server = app.listen(port, () => {
    const addr = server.address();
	console.log(`Server listening at http://${addr.address}:${addr.port}`);
})
